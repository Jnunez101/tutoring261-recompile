1. Title
2. Title again
3. This contains code showing how to do for and while loops in C++. The syntax
   is basically the same as Java. These two loops should do the exact same
   thing.

4. This is a bit more complicated. The first for loop is doing the same things
   as the other two but uses the range based syntax. This is equivalent to the
   for each syntax in Java and is similar to for loops in Python. The second
   example is using iterators to do the same thing the range based loop is
   doing. This one is super verbose and throws out some weird concepts, but it
   tends to come up when looking at other people's code and code in the standard
   library. All of the loops on this page are equivalent to the ones on the
   previous page. It's worth pointing out that despite doing the same thing,
   some of the loops are really hard to read. Try and get students thinking
   about when they would pick one loop type over another.
5. This is just covering some of the weird stuff that came up in the previous
   slide. When I say C++ has iterator loops at the library level, I mean that
   iterator loops are the exact same syntactically as the for i loops. It's just
   that instead of incrementing some integer, you increment what C++ calls an
   iterator. The keyword auto lets C++ try to guess the type. It doesn't always
   work though.
6. Same as 5
7. Just added this slide to make sure people don't immediately discount iterator
   loops as being overly complex. They have their use and it's worth having them
   in the back of your mind while writing code.
8. Same as 7
9. This example shown recursion. The syntax is nearly identical to Java.
10. This is the challenge for the students. The goal is to write code that
    prints n numbers where n is the number the user gave. The numbers you print
    should be 0-9 cycled. For example, if the user gives you 3, you should print
    "0 1 2". If the user gave you 10, you would print "0 1 2 3 4 5 6 7 8 9 10".
    If the user gave you 12, you would print "0 1 2 3 4 5 6 7 8 9 0 1 2".
    Writing this using ranges and iterators is actually really hard and people
    will probably need help with that.

    The more I look at this challenge and the solution I wrote, the less I like
    it. I think the idea of having students use the different kinds of loops is
    good, but the solution I came up with used While loops in conjunction with
    range/iterator loops and that kind of feels like cheating. Maybe a different
    challenge would be ideal that shown how iterator/range loops have an
    advantage?
