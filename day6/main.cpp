#include <iostream>
#include <sstream>

using namespace std;

template <class T>
void printf(string s, T t) {
  stringstream tAsStringStream;
  tAsStringStream << t;
  string tAsString = tAsStringStream.str();
  while (s.find('$') != string::npos) {
    s.replace(s.find('$'), 1, tAsString);
  }
  cout << s << endl;
}

int main() {
  printf<int>("hi $ bye", 5);
  printf<string>("hi $ $ bye", "test");
}
