Stack section:
For people coming from Java, this info seems relevant. They will be used to
making dynamic arrays all the time but C++ will give them trouble. The idea here
is to explain why C++ is giving them trouble.

I used the slides to visualize the stack assuming code starts in main with 3
variables, calls function 1, then calls function 2. When the functions finish,
you can move the slides backwards to show things getting popped off the stack.

Pointers section:
Basically just explain pointers and how they give you multiple ways to access
the same things. Also show what the stack looks like for this simple example
with just a main function.

Arrays:
The idea here is to show how simple arrays work in C++. It's extremely similar
to Java. The example of the stack shows that the array itself is stored on the
stack along with a pointer to the first element in the array.

The next two slides shows how pointers are equivalent to arrays.

In this example, it's worth pointing out the new keyword and how they've
probably seen it in Java for making classes. Saying it does the same thing as
Java is probably a simplification, but I think it's a reasonable one to make.


Vectors:
Gives an analogy between Java and C++ ArrayList and Vector.

Challenge:
Asks the user to fill an array with an unknown number of integers. This one is
hard because you have to resize the array as you make it.  The goal here is to
implement a resizing dynamic array that doesn't leak memory.  If people are
struggling, you can change the problem to instead have the user type the number
of values in the array instead of using 0 to denote the end. Maybe explore what
made that easier.
