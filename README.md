This repository contains slides I used for helping people move from Java to C++.
The last couple of days move a bit into 262 territory but the content seemed
useful since they might have already seen the concepts in Java.

To build the slides, run the command `pdflatex --shell-escape fileName.tex` in
the day's directory on Linux/Mac. Latex can be an absolute pain to get working
sometimes so if you're trying to modify/compile the slides and run into
problems, let me know and I'll try and help. Some of the filenames are
`bash.tex` only because I used slides on Bash as the template for these ones.

Usually I would introduce some material, provide an example on my machine,
then provide some kind of practice challenge for them to do. There usually
weren't too many people so it was usually pretty easy to give everyone help
during that time. At the end of the session, I went over my solution on the
board and did a final check for questions.

A lot of the slides jump into code pretty fast so it's worth giving some context
on the subtitle/title slides before moving on.
