1. Title
2. Subtitle
3. Comparing C++ and Java code that print hello world. At this point, I asked
   students to list the similarities and differences between the two examples.

The next several lines list some possible answers


8. Comparing reading input in C++ and Java. It might be worth pointing out the
   similarity between cout and cin syntax and that C++ doesn't return the line
   but instead fills in a variable.

9. Just goes over how Java/C++ types line up.

11. Challenge for this day.
