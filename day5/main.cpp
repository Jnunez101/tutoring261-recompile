#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

vector<int> operator+(vector<int> a, vector<int> b)  {
  for (auto x : b) {
    a.push_back(x);
  }
  return a;
}

ostream& operator<<(ostream& os, const vector<int>& b)  {
  os << "[";
  for (auto x : b) {
    os << x << ",";
  }
  os << "]";
  return os;
}
class Complex {
  public:
  int r;
  int i;
  Complex operator*(Complex b) {
    return Complex(r * b.r, i * b.i);
  }

  Complex operator+(Complex b) {
    return Complex(r + b.r, i + b.i);
  }

  Complex operator-(Complex b) {
    return Complex(r - b.r, i - b.i);
  }

  Complex operator/(Complex b) {
    return Complex(r / b.r, i / b.i);
  }

  Complex(int r1, int i1) {
    r = r1;
    i = i1;
  }
};

ostream& operator<<(ostream& os, Complex c) {
  return os << c.r << " + " << c.i << "i";
}

istream& operator>>(istream& os, Complex& c) {
  return os >> c.r >> c.i;
}

int main() {
  Complex c1(2, 1);
  Complex c2(0, 1);
  cin >> c2;
  cout << (c1 * c2) << endl;
  return 0;
}
