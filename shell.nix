let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python37.withPackages (ps: with ps; [pygments]))

  ];
}
